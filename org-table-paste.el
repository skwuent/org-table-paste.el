(defun paste-buffer-lines-to-this-column () ;; make it nullary for now...
  "take the lines in buffer x and put each line into a cell of the org-mode table at point."
  (interactive)
  (switch-to-buffer "*scratch*")
  (count-lines (point-min) (point-max))
  )
  ;; count lines in the source buffer
  ;; count rows below POINT in the table, including the current row
  ;; check that number of lines in source buffer is equal or fewer than remaining rows in the table


  ;; is there an org-mode function to go to the next lower cell in the column where point is at?
  ;; yes, org-table-next-row
  ;; what about org-table-iterate?


    )
